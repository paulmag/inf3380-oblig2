#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

void allocate_matrix (double*** matrix, int* n_rows, int* n_cols) {
    *matrix = (double**)malloc((*n_rows)*sizeof(double*));
    (*matrix)[0] = (double*)malloc((*n_rows)*(*n_cols)*sizeof(double));
    int i;
    for (i=1; i<(*n_rows); i++) {
        (*matrix)[i] = (*matrix)[i-1]+(*n_cols);
    }
}

void read_matrix_binaryformat (char* filename, double*** matrix,
                               int* n_rows, int* n_cols)
{
    FILE* fp = fopen (filename,"rb");
    int unused;  // Placeholder to avoid warning, since fread returns an int.
    unused = fread(n_rows, sizeof(int), 1, fp);
    unused = fread(n_cols, sizeof(int), 1, fp);

    /* storage allocation of the matrix */
    *matrix = (double**)malloc((*n_rows)*sizeof(double*));
    (*matrix)[0] = (double*)malloc((*n_rows)*(*n_cols)*sizeof(double));
    int i;
    for (i=1; i<(*n_rows); i++) {
        (*matrix)[i] = (*matrix)[i-1]+(*n_cols);
    }

    /* read in the entire matrix */
    unused = fread((*matrix)[0], sizeof(double), (*n_rows)*(*n_cols), fp);
    fclose(fp);
}

void write_matrix_binaryformat (char* filename, double** matrix,
                                int n_rows, int n_cols)
{
    FILE *fp = fopen (filename,"wb");
    fwrite(&n_rows, sizeof(int), 1, fp);
    fwrite(&n_cols, sizeof(int), 1, fp);
    fwrite(matrix[0], sizeof(double), n_rows*n_cols, fp);
    fclose(fp);
}

void multiply_matrix(
    double** a, double** b, double*** c, int m_start, int m_end, int l, int n)
{
    int i, j, k;
    double s;
    for (i=m_start; i<m_end; i++) {
        for (j=0; j<n; j++) {
            s = 0.;
            #pragma omp parallel for reduction(+:s) collapse(1)
            for (k=0; k<l; k++) { // This loop happens in parallel shared memory
                s += a[i][k] * b[k][j];
            }
            (*c)[i][j] = s;
        }
    }
}

void print_matrix(double** matrix, int m, int n) {
    int i, j;
    for (i=0; i<m; i++) {
        printf("%3d: ", i);
        for (j=0; j<n; j++) {
            printf("%10.3f", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main(int argc, char *argv[])
{
    int n1, n2, n3;
    int my_rank, num_procs;
    char *filename_a, *filename_b, *filename_c;
    double **matrix_a, **matrix_b, **matrix_c;
    int myrow_size, myrow_start, myrow_end;
    int *rowlist;

    filename_a = argv[1];
    filename_b = argv[2];
    filename_c = argv[3];
    n1 = atoi(argv[4]);
    n2 = atoi(argv[5]);
    n3 = atoi(argv[6]);

    /* Initialize MPI */
    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size (MPI_COMM_WORLD, &num_procs);

    /* Slice up the rows among the MPI processes */
    myrow_size = n1 / num_procs;
    myrow_start =  my_rank    * myrow_size;
    myrow_end   = (my_rank+1) * myrow_size;
    rowlist = (int*)malloc((num_procs+1) * sizeof(int));
    int rank, i;
    for (rank=0; rank<num_procs; rank++) {
        rowlist[rank] = rank * myrow_size;
    }
    rowlist[num_procs] = n1;
    if (my_rank == num_procs-1) {
        myrow_size += n1 % num_procs;
        myrow_end  += n1 % num_procs;
    }

    /* Read matrices A and B from file */
    if (my_rank == 0) {
        read_matrix_binaryformat(filename_a, &matrix_a, &n1, &n2);
        read_matrix_binaryformat(filename_b, &matrix_b, &n2, &n3);
    } else {
        /* Allocate space for A an B in the other processes */
        allocate_matrix(&matrix_a, &n1, &n2);
        allocate_matrix(&matrix_b, &n2, &n3);
    }

    /* Distribute matrices A and B to other processes */
    if (my_rank == 0) {
        for (rank=1; rank<num_procs; rank++) {
            /* Send chunks of A */
            for (i=rowlist[rank]; i<rowlist[rank+1]; i++) {
                MPI_Send(
                    matrix_a[i],
                    n2,
                    MPI_DOUBLE,
                    rank,  // destination rank
                    0,
                    MPI_COMM_WORLD
                );
            }
            /* Send entire B */
            MPI_Send(
                *matrix_b,
                n2 * n3,
                MPI_DOUBLE,
                rank,  // destination rank
                0,
                MPI_COMM_WORLD
            );
        }
    } else if (my_rank > 0) {
        /* Receive chunks of B */
        for (i=myrow_start; i<myrow_end; i++) {
            MPI_Recv(
                matrix_a[i],
                n2,
                MPI_DOUBLE,
                0,  // source rank
                0,
                MPI_COMM_WORLD,
                MPI_STATUS_IGNORE
            );
        }
        /* Receive entire B */
        MPI_Recv(
            *matrix_b,
            n2 * n3,
            MPI_DOUBLE,
            0,  // source rank
            0,
            MPI_COMM_WORLD,
            MPI_STATUS_IGNORE
        );
    }

    /* Do the matrix multiplication */
    allocate_matrix(&matrix_c, &n1, &n3);
    multiply_matrix(matrix_a, matrix_b, &matrix_c, myrow_start, myrow_end, n2, n3);

    /* Gather resulting matrix C chunks from other processes */
    if (my_rank > 0) {
        for (i=myrow_start; i<myrow_end; i++) {
            MPI_Send(
                matrix_c[i],
                n3,
                MPI_DOUBLE,
                0,  // destination rank
                0,
                MPI_COMM_WORLD
            );
        }
    } else if (my_rank == 0) {
        for (rank=1; rank<num_procs; rank++) {
            for (i=rowlist[rank]; i<rowlist[rank+1]; i++) {
                MPI_Recv(
                    matrix_c[i],
                    n3,
                    MPI_DOUBLE,
                    rank,  // source rank
                    0,
                    MPI_COMM_WORLD,
                    MPI_STATUS_IGNORE
                );
            }
        }
    }
    // Print the first 6 columns of matrix C:
    // if (my_rank == 0) {print_matrix(matrix_c, n1, 6);}

    /* Finalize */
    if (my_rank == 0) {
        write_matrix_binaryformat(filename_c, matrix_c, n1, n3);
    }
    printf("Process rank %d of %d is done. \n", my_rank, num_procs);
    MPI_Finalize();
    return 0;
}
