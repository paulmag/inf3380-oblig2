Mandatory exercise number 2 in INF3380 spring 2016.
Author: Paul Magnus Sørensen-Clark
Email: paulms@student.matnat.uio.no

How to compile:
mpicc -openmp main.c -o main.o

Usage:
mpirun -n num_procs ./main.o matrix_a.bin matrix_b.bin matrix_c.bin m l n

This program takes 6 cmd-line args which are all mandatory. The first 2 are the
filenames for 2 already existing binary files which must contain the matrices A
and B. The shape of A must be (m, l) and the shape of B must be (l, n), such
that they are compatible with eachother for a matrix multiplication. The third
arg is the desired filename for the output matrix C, which will be the result
of the matrix multiplication A*B=C. The shape of C will always be (m, n). The 3
last arguments m, l, and n must be the shapes of matrices A and B and will thus
also determine the shape of C.

Sample run:
"""
[paulms@kitalpha oblig2]$ mpicc -openmp main.c -o main.o
[paulms@kitalpha oblig2]$ mpirun -n 16 ./main.o data/large_matrix_a.bin data/large_matrix_b.bin data/large_matrix_c.bin 1000 500 1000
Process rank 1 of 16 is done.
Process rank 2 of 16 is done.
Process rank 3 of 16 is done.
Process rank 4 of 16 is done.
Process rank 5 of 16 is done.
Process rank 6 of 16 is done.
Process rank 7 of 16 is done.
Process rank 8 of 16 is done.
Process rank 9 of 16 is done.
Process rank 10 of 16 is done.
Process rank 11 of 16 is done.
Process rank 12 of 16 is done.
Process rank 13 of 16 is done.
Process rank 14 of 16 is done.
Process rank 15 of 16 is done.
Process rank 0 of 16 is done.
"""
